package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedidos;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena(){
        String esperado = "L.U.Z D.E.L S.O.L";
        assertEquals(esperado,empleadosController.getCadena("luz del sol","."));

    }
    @Test
    public void testBadSeparator(){
        try{
            Utilidades.getCadena("Oscar Llamas", "..");
            fail("Se esperaba badSeparator");
        }catch (BadSeparator bs){}
    }

    @ParameterizedTest
    @ValueSource(ints = {1,3,5,-3,15, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {""," "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    public void testEstaBlancoNull(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {""," "})
    public void testEstaBlancoNull2(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedidos.class)
    public void testValorarEstadoPedido(EstadosPedidos estado){
        assertTrue(Utilidades.valorarEstadoPedido(estado));
    }
}
