package com.techuniversity.emp.utils;

public enum EstadosPedidos {
    ACEPTADO,
    COCINADO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO,
    COMIDO;
}
